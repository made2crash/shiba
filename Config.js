let packageJson = require('./package.json');

/* eslint no-process-env: 0 */
module.exports =
{
  ENV:            process.env.NODE_ENV || 'development',
  VERSION:        packageJson.version,
  CLIENT_SEED:
    process.env.BUSTABIT_CLIENTSEED ||
    '0000000000010f4889163236c4ff64863e366ef241f53ea3478a85b3915f466f',
  GAMESERVER:     process.env.BUSTABIT_GAMESERVER || 'http://localhost:9095',
  WEBSERVER:      process.env.BUSTABIT_WEBSERVER || 'http://localhost:9094',
  OXR_APP_ID:     process.env.OXR_APP_ID,
  SESSION:        process.env.SHIBA_SESSION || '46a8d882-b529-412a-8d63-203bd4579687',
  DATABASE:       process.env.SHIBA_DATABASE || 'postgres://user@localhost:5432/passHere',
  CHAT_HISTORY:   process.env.SHIBA_CHAT_HISTORY || 5000,
  GAME_HISTORY:   process.env.SHIBA_GAME_HISTORY || 500,
  /* keep in lowercase */
  USER_WHITELIST: [
    'Bird',
    'gideonpraise',
    'bird'
  ]
};
